﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.Services
{
    public class LettersConverterService : ILetterConverterService
    {
 
        public string DeleteUnecessary(string letters)
        {
            int lastValue = GetIntValue(letters[0]);
            int count = 0;
            string cleanLetters = "";

            foreach (char l in letters)
            {
                if (GetIntValue(l) >= lastValue)
                {
                    cleanLetters = cleanLetters + letters[count];
                }
                count += 1;
                lastValue = GetIntValue(l);
            }
            return cleanLetters;
        }

        public int GetIntValue(char letter)
        {
            return (char.ToUpper(letter) - 64);
        }

        public static int GetSum(int lastValue, int sum)
        {
            return (((lastValue * sum) * sum) - lastValue);
        }

        public int CalculateString(string letters)
        {
            int total = 0;
            string cleanLetters = DeleteUnecessary(letters);
            int lastValue = -1;
            int sum = 1;
            int count = 0;
            foreach (char l in cleanLetters)
            {
                if (count == cleanLetters.Length - 1)
                {
                    if (GetIntValue(l) == lastValue)
                    {
                        sum += 1;
                        total += GetSum(lastValue, sum);
                    }
                    else
                    {
                        total += GetIntValue(l);
                        total += GetSum(lastValue, sum);
                    }
                }
                else
                {
                    if (GetIntValue(l) == lastValue)
                    {
                        sum += 1;
                    }
                    else
                    {
                        if (sum == 1)
                        {
                            total += GetIntValue(l);
                        }
                        else
                        {
                            total += GetSum(lastValue, sum);
                            sum = 1;
                        }
                    }
                }
                lastValue = GetIntValue(l);
                count++;
            }
            return total;
        }
    }
    public interface ILetterConverterService
    {
        int CalculateString(string letters);
    }
}

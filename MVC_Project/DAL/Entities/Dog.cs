﻿using MVC_Project.Code.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.DAL.Entities
{
    public class Dog
    {
        public int Id { get; set; }
        public DogName Name { get; set; }

        public DogBreed Breed { get; set; }
    }
}

﻿using MVC_Project.Code.Enum;
using MVC_Project.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.Code.Repos
{
    public interface IDogRepo
    {
        IEnumerable<Dog> GetAll();
        Dog GetDogById(int id);
        Dog GetByName(DogName name);
        Dog UpdateDog(Dog dog);
        Dog CreateNewRandomDog();
        bool KillDog(int? id);
        IEnumerable<Dog> GetByBreed(DogBreed race);
        int KillDogsByBreed(DogBreed race = DogBreed.Pug);
    }
}

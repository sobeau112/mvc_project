﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVC_Project.Code.Enum;
using MVC_Project.DAL.Entities;

namespace MVC_Project.Code.Repos
{
    public class DogRepo : IDogRepo
    {
        public static List<Dog> BD = new List<Dog>();

        public Dog CreateNewRandomDog()
        {
            //Cree des chiens random (fait une banque de nom, pi race random!)

            Array allBreed = Enum.DogBreed.GetValues(typeof(DogBreed));
            Array allName = Enum.DogName.GetValues(typeof(DogName));
            Random random = new Random();
            DogBreed randomBreed = (DogBreed)allBreed.GetValue(random.Next(allBreed.Length));
            DogName randomName = (DogName)allName.GetValue(random.Next(allName.Length));

            int id = (BD.Count > 0) ? BD[BD.Count - 1].Id + 1 : 1;
            
            var newDog = new Dog()
            {
                Id = id,
                Breed = randomBreed,
                Name = randomName
            };

            BD.Add(newDog);
            return newDog;
        }

        public IEnumerable<Dog> GetAll()
        {
            return BD;
        }

        public Dog GetDogById(int id)
        {
            return BD.FirstOrDefault(dog => { return dog.Id == id; });
        }

        public Dog GetByName(DogName name)
        {
            return BD.FirstOrDefault(dog =>
            { 
                return dog.Name == name; 
            });  
        }

        public IEnumerable<Dog> GetByBreed(DogBreed race)
        {
            return BD.FindAll(dog =>
            {
                return dog.Breed == race;
            });
        }

        public bool KillDog(int? id)
        {
            int numberOfVictims = BD.RemoveAll(dog => dog.Id == id);
            if (numberOfVictims > 0)
            {
                return true;

            }
            return false;
        }

        /// <summary>
        ///Tue les chiens et retourne le nombre de victime! 
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public int KillDogsByBreed(DogBreed breed)
        {
            return BD.RemoveAll(dog => dog.Breed == breed);
        }

        public Dog UpdateDog(Dog newDog)
        {
            int index = -1;
            index = BD.FindIndex(dog => dog.Id == newDog.Id);
            if (index != -1)
            {
                BD[index].Name = newDog.Name;
                BD[index].Breed = newDog.Breed;
            }
            else
            {
                BD.Add(newDog);
            }
            return newDog;
        }
    }
}

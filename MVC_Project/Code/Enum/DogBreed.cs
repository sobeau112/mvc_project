﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.Code.Enum
{
    public enum DogBreed
    {
        Shepperd,
        Pug,
        Eskimo,
        Terrier,
        Spaniel,
        Beagle,
        Samoyed,
        Husky
    }
}

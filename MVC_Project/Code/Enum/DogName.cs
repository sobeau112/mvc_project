﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.Code.Enum
{
    public enum DogName
    {
        Ace,
        Apollo,
        Bailey,
        Bandit,Baxter,
        Bear,
        Beau,
        Benji,
        Benny,
        Bentley,
        Blue,
        Bo,
        Boomer,
        Brady,
        Brody,
        Bruno,
        Brutus,
        Bubba,
        Buddy,
        Buster,
        Cash,
        Champ,
        Chance,
        Chase,
        Chester,
        Chico,
        Coco,
        Cody,
        Cooper
    }
}

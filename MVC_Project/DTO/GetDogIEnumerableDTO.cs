﻿using MVC_Project.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.DTO
{
    public class GetDogIEnumerableDTO
    {
        public IEnumerable<Dog> Dog { get; set; }
    }
}

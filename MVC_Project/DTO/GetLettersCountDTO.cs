﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Project.DTO
{
    public class GetLettersCountDTO
    {
        public int Count { get; set; }
    }
}

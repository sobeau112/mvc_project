﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC_Project.Code.Enum;
using MVC_Project.Code.Repos;
using MVC_Project.DAL.Entities;
using MVC_Project.DTO;
using MVC_Project.Services;
using System.Linq;
namespace MVC_Project.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DogController : ControllerBase
    {
        private readonly ILogger<DogController> _logger;
        private readonly ILetterConverterService _letterConverterService;
        private readonly IDogRepo _dogRepo;
        private object context;

        public DogController(ILogger<DogController> logger, ILetterConverterService letterConverterService, IDogRepo dogRepo)
        {
            _logger = logger;
            _letterConverterService = letterConverterService;
            _dogRepo = dogRepo;
        }

        [HttpGet("LettersConverter")]
        public IActionResult GetLettersCount(string letters)
        {
            if (letters == null)
            {
                return new BadRequestObjectResult("Enter letters");
            }
            var stringCount = _letterConverterService.CalculateString(letters);
            var resp = new GetLettersCountDTO() { Count = stringCount };
            if (stringCount > 30)
            {
                return new BadRequestObjectResult(resp);
            }
            return new OkObjectResult(resp);
        }

        [HttpGet]
        public IActionResult GETGetAllDogs()
        {
                IEnumerable<Dog> dog = _dogRepo.GetAll();
                if (dog.FirstOrDefault() == null)
                {
                    return new OkObjectResult("The dog list is empty");
                }
                var resp = new GetDogIEnumerableDTO() { Dog = dog };
                return new OkObjectResult(resp);
        }

        [HttpGet ("{id}")]
        public IActionResult GETGetDogById(int id)
        {
            Dog dog = _dogRepo.GetDogById(id);
            if (dog == null)
            {
                return new OkObjectResult("This id is not related to any dog");
            }
            var resp = new GetDogDTO() { Dog = dog };
            return new OkObjectResult(resp);
        }

        [HttpPost]
        public IActionResult POSTCreateNewRandomDog()
        {
            Dog newDog = _dogRepo.CreateNewRandomDog();
            return new OkObjectResult(newDog);
        }

        [HttpGet("DogRepo/GetByName")]
        public IActionResult GETGetByName(string strDogName)
        {
            if (strDogName == null)
            {
                return new OkObjectResult("Enter a dog name");
            }
            try
            {
                DogName dogName = (DogName)Enum.Parse(typeof(DogName), strDogName);
                var dog = _dogRepo.GetByName(dogName);
                if (dog == null)
                {
                    return new BadRequestObjectResult("Enter an existing dog name");
                }
                var resp = new GetDogDTO() { Dog = dog };
                return new OkObjectResult(resp);
            }
            catch (Exception)
            {
                return new BadRequestObjectResult("Enter a valid dog name");
            }
        }

        [HttpGet("DogRepo/GetByBreed")]
        public IActionResult GETGetByBreed(string strDogBreed)
        {
            if (strDogBreed == null)
            {
                return new OkObjectResult("Enter a dog breed");
            }
            try
            {
                DogBreed dogBreed = (DogBreed)Enum.Parse(typeof(DogBreed), strDogBreed);
                var dog = _dogRepo.GetByBreed(dogBreed);
                List<Dog> dogList = dog.ToList();
                if (dog.FirstOrDefault() == null)
                {
                    return new BadRequestObjectResult("Enter an existing dog breed");
                }
                var resp = new GetDogIEnumerableDTO() { Dog = dog };
                return new OkObjectResult(resp);
            }
            catch (Exception)
            {
                return new BadRequestObjectResult("Enter a valid dog breed");
            }
        }

        [HttpDelete("DogRepo/KillDog")]
        public IActionResult DELETEKillDog(int? id)
        {
            if (id == null)
            {
                return new OkObjectResult("Enter a dog ID");
            }
            if (_dogRepo.KillDog(id))
            {
                return new OkObjectResult("One dog was deleted");
            }
            return new BadRequestObjectResult("This id is not related to any dog");
        }

        [HttpDelete("DogRepo/KillDogsByBreed")]
        public IActionResult DELETEKillDogsByBreed(string strDogBreed)
        {
            if (strDogBreed == null)
            {
                return new OkObjectResult("Enter a breed");
            }
            try
            {
                DogBreed dogBreed = (DogBreed)Enum.Parse(typeof(DogBreed), strDogBreed);
                int numberOfVictims = _dogRepo.KillDogsByBreed(dogBreed);
                if (numberOfVictims == 0)
                {
                    return new BadRequestObjectResult("Enter an existing dog breed");
                }
                return new OkObjectResult(numberOfVictims.ToString() + " Victims");
            }
            catch (Exception)
            {
                return new BadRequestObjectResult("Enter a valid dog breed");
            }
        }

        [HttpPut("DogRepo/UpdateDog")]
        public IActionResult PUTUpdateDog(int? nullableId, string strName, string strBreed)
        {
            if (nullableId == null || strName == null || strBreed == null)
            {
                return new BadRequestObjectResult("You must enter a value for id, name and breed");
            }
            try
            {
                int id = nullableId ?? 0;
                id = id <= 0 ? id * -1 : id;
                DogName dogName = (DogName)Enum.Parse(typeof(DogName), strName);
                DogBreed dogBreed = (DogBreed)Enum.Parse(typeof(DogBreed), strBreed);
                var newDog = new Dog()
                {
                    Id = id,
                    Breed = dogBreed,
                    Name = dogName
                };
                Dog UpdatedDog = _dogRepo.UpdateDog(newDog);
                return new OkObjectResult(newDog);
            }
            catch (Exception)
            {
                return new BadRequestObjectResult("Enter valid dog name and breed");
            }
        }

    }
}

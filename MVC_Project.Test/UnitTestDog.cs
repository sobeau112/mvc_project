using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC_Project.Code.Enum;
using MVC_Project.Code.Repos;
using MVC_Project.DAL.Entities;
using System;
using System.Collections.Generic;

namespace MVC_Project.Test
{
    [TestClass]
    public class UnitTestDog
    {
        [TestMethod]
        public void TestAddNewDogToBD()
        {
            // Y a t'il une autre fa�on de tester les randoms? Ici je ne regarde pas leur valeurs, mais leur type
            // Est-ce que c'est correct de mettre plusieurs assert dans le m�me test si c'est pour tester la m�me methode?
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            Dog dog = dogRepo.CreateNewRandomDog();
            Assert.AreEqual(1, BD.Count);
            Assert.AreEqual(1, BD[0].Id);
            Assert.AreEqual(typeof(DogName), BD[0].Name.GetType());
            Assert.AreEqual(typeof(DogBreed), BD[0].Breed.GetType());
            BD.Clear();
        }

        [TestMethod]
        public void TestCreateSecondNewDog()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            Dog dog = dogRepo.CreateNewRandomDog();
            Dog dog2 = dogRepo.CreateNewRandomDog();
            Assert.AreEqual(2, BD.Count);
            BD.Clear();
        }

        [TestMethod]
        public void TestGetAllDogs()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Ace
            };
            BD.Add(newDog);
            var newDog2 = new Dog()
            {
                Id = 2,
                Breed = DogBreed.Eskimo,
                Name = DogName.Apollo
            };
            BD.Add(newDog2);
            IEnumerable<Dog> dogs = dogRepo.GetAll();
            Assert.AreEqual(dogs, BD);
            BD.Clear();
        }

        [TestMethod]
        public void TestGetDogById()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Eskimo,
                Name = DogName.Apollo
            };
            BD.Add(newDog);
            Dog dog = dogRepo.GetDogById(1);
            Assert.AreEqual(BD[0], dog);
            BD.Clear();
        }

        [TestMethod]
        public void TestGetDogByName()
        {
            // Ici je n'utilise pas la methode qui fait des create random pour pouvoir controler les �l�ments de la liste.
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Ace
            };
            BD.Add(newDog);
            var newDog2 = new Dog()
            {
                Id = 2,
                Breed = DogBreed.Spaniel,
                Name = DogName.Bear
            };
            BD.Add(newDog2);
            Dog dog = dogRepo.GetByName(DogName.Bear);
            Assert.AreEqual(newDog2, BD[1]);
            BD.Clear();
        }

        [TestMethod]
        public void TestGetDogOfbreed()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Ace
            };
            BD.Add(newDog);
            var newDog2 = new Dog()
            {
                Id = 2,
                Breed = DogBreed.Pug,
                Name = DogName.Apollo
            };
            BD.Add(newDog2);
            IEnumerable<Dog> dog = dogRepo.GetByBreed(DogBreed.Pug);
            Assert.AreEqual(newDog2, BD[1]);
            BD.Clear();
        }

        [TestMethod]
        public void TestKillDog()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            dogRepo.CreateNewRandomDog();
            dogRepo.CreateNewRandomDog();
            dogRepo.KillDog(1);
            Assert.AreEqual(1, BD.Count);
            BD.Clear();
        }

        [TestMethod]
        public void TestKillDogOfRace()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Ace
            };
            BD.Add(newDog);
            var newDog2 = new Dog()
            {
                Id = 2,
                Breed = DogBreed.Pug,
                Name = DogName.Apollo
            };
            BD.Add(newDog2);
            int victimCount = dogRepo.KillDogsByBreed(DogBreed.Pug);
            Assert.AreEqual(1, BD.Count);
            Assert.AreEqual(DogBreed.Beagle, BD[0].Breed);
            Assert.AreEqual(1, victimCount);
            BD.Clear();
        }

        [TestMethod]
        public void TestUpdateExistingDog()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Apollo
            };
            
            var existingDog1 = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Ace
            };
            BD.Add(existingDog1);
            var existingDog2 = new Dog()
            {
                Id = 2,
                Breed = DogBreed.Eskimo,
                Name = DogName.Brutus
            };
            BD.Add(existingDog2);
            dogRepo.UpdateDog(newDog);
            Assert.AreEqual(2, BD.Count);
            Assert.AreEqual(DogName.Apollo, BD[0].Name);
            BD.Clear();
        }

        [TestMethod]
        public void TestUpdateNewDog()
        {
            DogRepo dogRepo = new DogRepo();
            var BD = DogRepo.BD;
            var newDog = new Dog()
            {
                Id = 3,
                Breed = DogBreed.Beagle,
                Name = DogName.Apollo
            };

            var existingDog1 = new Dog()
            {
                Id = 1,
                Breed = DogBreed.Beagle,
                Name = DogName.Ace
            };
            BD.Add(existingDog1);
            var existingDog2 = new Dog()
            {
                Id = 2,
                Breed = DogBreed.Eskimo,
                Name = DogName.Brutus
            };
            BD.Add(existingDog2);
            dogRepo.UpdateDog(newDog);
            Assert.AreEqual(3, BD.Count);
            Assert.AreEqual(DogName.Ace, BD[0].Name);
        }

    }
     
}

